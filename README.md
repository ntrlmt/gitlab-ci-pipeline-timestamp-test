# GitLab CI Pipeline Timestamp Test

This package is get a time stamp example that GitLab CI pipeline starts.


# Details

Change $CI_PIPELINE_CREATED_AT to YYYYmmddHHMM timestamp with the date command.

* CI_PIPELINE_CREATED_AT
  * GitLab CI pipeline start timestamp with ISO 8601 ([reference](https://docs.gitlab.com/ee/ci/variables/predefined_variables.html)).


The date command with option **-d** supports ISO 8601 (v8.13 or later: [reference](https://stackoverflow.com/questions/21122170/date-d-doesnt-accept-iso-8601-format)).

Example: a timestamp output is YYYYmmddHHMM with TZ="Asia/Tokyo".
```bash
TZ="Asia/Tokyo" date -d "${CI_PIPELINE_CREATED_AT}" +"%Y%m%d%H%M"
```

Output (From 2023-12-27T00:00:38Z to YYYYmmddHHMM)
```bash
202312270900
```

The complete CI code shows below.  
[.gitlab-ci.yml](.gitlab-ci.yml)
```yaml
stages:
  - build

build-job:
  image: ubuntu:latest
  stage: build
  variables:
    - DEBIAN_FRONTEND: noninteractive
  before_script:
    - echo "CI pipeline created timestamp:" ${CI_PIPELINE_CREATED_AT} 
    - apt-get update && apt install -y tzdata
    - export PIPELINE_TIMESTAMP=$(TZ="Asia/Tokyo" date -d "${CI_PIPELINE_CREATED_AT}" +"%Y%m%d%H%M")
  script:
    - echo "Pipeline timestamp (YYYYmmddHHMM)" $PIPELINE_TIMESTAMP
```